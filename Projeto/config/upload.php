<?php

    $_UP['pasta'] = '../../imagens/';
    $_UP['tamanho'] = 1024*1024*2;
    $_UP['extensoes'] = array('jpg','png');
    $_UP['renomeia'] = true;
    $_UP['sucesso'] = 'Imagem inserida com sucesso';


    $_UP['erros'][0] = 'sem erro';
    $_UP['erros'][1] = 'O arquivo é maior que o limite do PHP';
    $_UP['erros'][2] = 'O arquivo é maior que o especificado no HTML';
    $_UP['erros'][3] = 'Upload feito parcialmente';
    $_UP['erros'][4] = 'Não foi feito upload';


    $_UP['erros']['extensao'] = 'A extensão solicitada é incorreta';
    $_UP['erros']['tamanho'] = 'Tamanho excede o limite permitido';
    $_UP['erros']['padrao'] = 'Ocorreu um erro, tente novamente';

    function verificaErro(){
        global $_UP;
        global $_nomeFinal;
        
        if($_FILES['foto']['error'] != 0){
            return $_UP['erros'][$_FILES['foto']['error']];
            
        }
        
        $ext = explode('.',$_FILES['foto']['name']);
        
        $extensao = strtolower(end($ext));
        
        if(array_search($extensao,$_UP['extensoes']) === false){
            return $_UP['erros']['extensao'];
        } 
        else if($_UP['tamanho'] < $_FILES['foto']['size']){
            return $_UP['erros']['tamanho'];
        }
        else{
            if($_UP['renomeia'] == true){
                $_nomeFinal = time().'.jpg';
            }
            else{
                $_nomeFinal = $_FILES['foto']['name'];
            }
        }
        
        if(move_uploaded_file($_FILES['foto']['tmp_name'],$_UP['pasta'].$_nomeFinal)){
            return false;
        }
        else{
            return $_UP['erros']['padrao'];
        }
        
        
    }


?>