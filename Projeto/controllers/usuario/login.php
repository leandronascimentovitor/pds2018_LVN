
<?php

include_once '../../models/entidades/usuario.php';
include_once '../../models/modelUsuario.php';
include_once '../../config/constantes.php';
include_once '../../views/compartilhado/cabecalho_limpo.php';

$email=$_POST['email'];
$senha=$_POST['senha'];
$usuario= new modelUsuario();
$resposta = $usuario->login($email,$senha);

if(empty($resposta)){
    session_unset();
    //echo '<script>window.location.href="'.BASE_URL.'views/login.php" </script>';
     echo '<script>swal("Não foi possível realizar o login", "Tente novamente", "error").then((value) => {
            window.location.href="'.BASE_URL.'views/login.php";
        }); </script>';
}
else{
    $sessaoArray = array($resposta['IDUsuario'],$resposta['nome'],$resposta['email']);
    $_SESSION['logado']= $sessaoArray;
    
     //$_SESSION['logado']= $resposta['IDUsuario'];
    
    //echo '<script>window.location.href="'.BASE_URL.'views/index.php" </script>';
     echo '<script>swal("Login realizado com sucesso.", "Seja bem vindo!", "success").then((value) => {
            window.location.href="'.BASE_URL.'views/index.php";
        }); </script>';
}



?>