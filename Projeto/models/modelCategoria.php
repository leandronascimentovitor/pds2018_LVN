<?php

include_once 'base.php';
include_once 'entidades/categoria.php';


class ModelCategoria{
    
    public function listarCategoria(){
       
        
        $conexao = abrirconexao();
        
        
        
        $query = "SELECT * FROM categoria";
        
        
        $executa = mysqli_query($conexao,$query);
        
     
        
        while($linha = mysqli_fetch_assoc($executa )){
               
            $resultado[] = $linha;
        }
        
        fecharConexao($conexao);
        
        
        if(isset($resultado)){
            
            $listaRetorno = array();
            
            foreach($resultado as $row){
                
                $categoria = new categoria();
                
                $categoria -> IDCategoria = $row['IDCategoria'];
                $categoria -> NomeCategoria = $row['NomeCategoria'];
                
                $listaRetorno[] = $categoria;
                
            }
            
            return $listaRetorno;
            
        }
    }
}

?>