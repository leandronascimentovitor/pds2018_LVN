<?php

include_once 'base.php';
include_once 'entidades/produto.php';


class modelProduto{
    
    public function cadastrarProduto(produto $product){
        $nomeProduto = $product->nomeProduto;
        $descricao = $product->descricao;
        $preco = $product->preco;
        $situacaoProduto = $product->IDSituacaoProduto;
        $categoria = $product->IDCategoria;
        $imagem = $product->Imagem;
        $dataAnuncio = $product->DataAnuncio;
        $idVendedor = $product->IDVendedor;
        
        
        $conexao = abrirconexao();
        
        $query = "INSERT INTO produto(nome,descricao,preco,IDSituacaoProduto,IDCategoria,Imagem,DataAnuncio,IDVendedor) VALUES('$nomeProduto','$descricao','$preco','$situacaoProduto','$categoria','$imagem','$dataAnuncio','$idVendedor');";
        
        $executa = mysqli_query($conexao,$query);
        
        $numeroLinhas = mysqli_affected_rows($conexao);
        fecharConexao($conexao);
        return $numeroLinhas;
        
    }
    
     
    public function contaTotalProduto(){
        
        
        
        $conexao = abrirconexao();
        
        $query = "SELECT COUNT(IDProduto) AS 'Quantidade' FROM produto WHERE DataVenda IS NULL AND IDComprador IS NULL;"; 
        
        
        $executa = mysqli_query($conexao,$query);
        
        $resultado = mysqli_fetch_assoc($executa);
        
        fecharConexao($conexao);
        return $resultado['Quantidade'];
        
    }
    
     public function contaTotalProdutoNome($nome,$IDs,$IDc){
        
        
        
        $conexao = abrirconexao();
        
        $query1 = "SELECT COUNT(IDProduto) AS 'Quantidade' FROM produto WHERE DataVenda IS NULL AND IDComprador IS NULL "; 
        
         if($IDs != 0){
            $query3 = " AND IDSituacaoProduto = $IDs";
            $query1 = $query1.$query3;
        }
        
        if($IDc != 0){
            $query4 = " AND IDCategoria = $IDc";
            $query1 = $query1.$query4;
        }
        
         $query2 = " AND nome like '%$nome%';";
        $query = $query1.$query2;
        $executa = mysqli_query($conexao,$query);
        
        $resultado = mysqli_fetch_assoc($executa);
        
        
        fecharConexao($conexao);
        return $resultado['Quantidade'];
        
    }
    
     public function retornaProdutos($incio, $quantidade_pg){
        
        
        
        $conexao = abrirconexao();
        
        $query1 = "SELECT categoria.NomeCategoria, situacaoproduto.NomeSituacao,produto.* from produto INNER JOIN situacaoproduto ON situacaoproduto.IDEstadoProduto = produto.IDSituacaoProduto INNER JOIN categoria ON categoria.IDCategoria=produto.IDCategoria WHERE DataVenda IS NULL AND IDComprador IS NULL ";
        
        $query2 = " limit $incio, $quantidade_pg;";
        $query = $query1.$query2;
         
        //echo $query;
        
        $executa = mysqli_query($conexao,$query);
        
        if(empty($executa)){
            $resultado = "";
        }
         else{
             while($linha=mysqli_fetch_assoc($executa)){
                 $resultado[] = $linha;
             }
         }
         
        fecharConexao($conexao);
         
        $retorno = null;
         
        if(isset($resultado)){
            $retorno = array();
            foreach($resultado as $row){
            $product = new produto();
        $product->IDProduto = $row['IDProduto'];    
        $product->nomeProduto = $row['nome'];
        $product->descricao = $row['descricao'];
        $product->preco = $row['preco'];
        $product->IDSituacaoProduto = $row['IDSituacaoProduto'];
        $product->IDCategoria = $row['IDCategoria'];
        $product->Imagem = $row['Imagem'];
        $product->DataAnuncio = $row['DataAnuncio'];
        $product->IDVendedor = $row['IDvendedor'];
        $product->NomeSituacao = $row['NomeSituacao'];
        $product->NomeCategoria = $row['NomeCategoria'];
        $retorno[] = $product;  
            }
        }
        
        return $retorno;
        
    }
    
    
    
         public function retornaProdutosID($codigo ){
        
        
        
        $conexao = abrirconexao();
        
        $query = "SELECT * FROM produto WHERE  DataVenda IS NULL AND IDComprador IS NULL AND IDProduto = $codigo;"; 
       // echo $query;
        
        $executa = mysqli_query($conexao,$query);
        
        if(empty($executa)){
            $resultado = "";
        }
         else{
             while($linha=mysqli_fetch_assoc($executa)){
                 $resultado[] = $linha;
             }
         }
         
        fecharConexao($conexao);
         
        $retorno = null;
         
        if(isset($resultado)){
            $retorno = array();
            foreach($resultado as $row){
            $product = new produto();
        $product->IDProduto = $row['IDProduto'];    
        $product->nomeProduto = $row['nome'];
        $product->descricao = $row['descricao'];
        $product->preco = $row['preco'];
        $product->IDSituacaoProduto = $row['IDSituacaoProduto'];
        $product->IDCategoria = $row['IDCategoria'];
        $product->Imagem = $row['Imagem'];
        $product->DataAnuncio = $row['DataAnuncio'];
        $product->IDVendedor = $row['IDvendedor'];
        $retorno[] = $product;  
            }
        }
        
        return $retorno;
        
    }
    
    public function retornaProdutosNome($incio, $quantidade_pg,$nome,$IDs,$IDc){
        
        
        
        $conexao = abrirconexao();
         $query1 = "SELECT categoria.NomeCategoria, situacaoproduto.NomeSituacao,produto.* from produto INNER JOIN situacaoproduto ON situacaoproduto.IDEstadoProduto = produto.IDSituacaoProduto INNER JOIN categoria ON categoria.IDCategoria=produto.IDCategoria WHERE DataVenda IS NULL AND IDComprador IS NULL AND nome like '%$nome%' ";
        
        if($IDs != 0){
            $query3 = " AND IDSituacaoProduto = $IDs";
            $query1 = $query1.$query3;
        }
        
        if($IDc != 0){
            $query4 = " AND produto.IDCategoria = $IDc";
            $query1 = $query1.$query4;
        }
        
        $query2 = " limit $incio, $quantidade_pg;";
        $query = $query1.$query2;
         
        //echo $query;
        
        $executa = mysqli_query($conexao,$query);
        
        if(empty($executa)){
            $resultado = "";
        }
         else{
             while($linha=mysqli_fetch_assoc($executa)){
                 $resultado[] = $linha;
             }
         }
         
        fecharConexao($conexao);
         
        $retorno = null;
         
        if(isset($resultado)){
            $retorno = array();
            foreach($resultado as $row){
            $product = new produto();
        $product->IDProduto = $row['IDProduto'];    
        $product->nomeProduto = $row['nome'];
        $product->descricao = $row['descricao'];
        $product->preco = $row['preco'];
        $product->IDSituacaoProduto = $row['IDSituacaoProduto'];
        $product->IDCategoria = $row['IDCategoria'];
        $product->Imagem = $row['Imagem'];
        $product->DataAnuncio = $row['DataAnuncio'];
        $product->IDVendedor = $row['IDvendedor'];
        $product->NomeSituacao = $row['NomeSituacao'];
        $product->NomeCategoria = $row['NomeCategoria'];
        $retorno[] = $product;  
            }
        }
        
        return $retorno;
        
    }
    
    public function compra($data,$IDc,$IDp){
             
         $conexao = abrirconexao();
         $query = "UPDATE produto SET DataVenda = '$data', IDComprador = '$IDc' WHERE IDProduto = '$IDp'";
        
        
       
         
        //echo $query;
        
        $executa = mysqli_query($conexao,$query);
     
        $linha = mysqli_affected_rows($conexao); 
     
        
        
        fecharConexao($conexao);
         
        
        
        return $linha;
        
        
    }
}