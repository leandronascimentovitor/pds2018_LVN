<?php
include_once '../config/constantes.php';
?>
<head>
        <meta charset="utf-8">
        
        <script src="https://ajax.googleapis.com/ajax/libs/hammerjs/2.0.8/hammer.min.js"></script>
        
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        
        <script src="<?php echo(JS.'sweetalert2.all.min.js'); ?>"></script>
    
        <link rel="stylesheet" type="text/css" href="<?php echo(CSS_URL.'estilo.css'); ?>">
    
</head>


<header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <a class="navbar-brand" href="<?php echo(BASE_URL.'views/'); ?>">
      <img src="<?php echo(IMG_URL.'logo.png'); ?>" alt="">
        </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav" id="menuItem">
     
      <li class="nav-item">
        <a class="nav-link" href="cadastro_produto.php">Inserir anúncio</a>
          
      </li>
      <li class="nav-item">
          <?php
          
          if(sizeof($_SESSION) > 0){
             
              echo '<a class="nav-link" href="../controllers/usuario/sair.php">Sair</a>';
          }
          else{
              echo '<a class="nav-link" href="login.php">Entre</a>';
          }
        //<a class="nav-link" href="login.php">Entre</a>
          ?>
      </li>
      
    </ul>
  </div>
</nav>
            

</header>