<?php 
include_once 'compartilhado/cabecalho.php';


?>
<html>

<head>
<title>Login e registro | NetVendas</title>
<meta charset="utf-8">

</head>

<body id="body-login">
    <div class="pagina-login">
        <div class="form">
            <form class="formulario-registro" name="signup" method="post" action="../controllers/usuario/inserir.php">
                <input type="text" placeholder="nome" name="nome"/>
                <input type="text" placeholder="email" name="email"/>
                <input type="password" placeholder="senha" name="senha"/>
                <button>Registrar</button>
                <p class="message">Já possui cadastro? <a href="#">Faça o login</a></p>
            </form>
            
            <form class="formulario-login" method="post" action="../controllers/usuario/login.php">
            <input type="text" placeholder="e-mail" name="email"/ >
            <input type="password" placeholder="Senha" name="senha"/>
            <button>Login</button>
            <p class="message">Não possui cadastro?<a href="#">Cadastre-se</a> </p>
                
            </form>
        </div>
    </div>
    
    <script src='https://code.jquery.com/jquery-3.3.1.min.js'>
    </script>
    
    <script>
    $('.message a').click(function(){
    $('form').animate({height:"toggle", opacity:"toggle"},"slow");    
    });
    </script>


</body>




</html>