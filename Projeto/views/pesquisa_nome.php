<?php

include_once 'compartilhado/cabecalho.php';
include_once '../controllers/produto/listar.php';
//include_once ''

$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
$nome = $_GET['nome'];
$IDs = $_GET['situacao'];
$IDc = $_GET['categoria'];
$total_produtos = contaTotalProdutoNome($nome,$IDs,$IDc);

$quantidade_pg = 8;

$num_pagina = ceil($total_produtos/$quantidade_pg);

$incio = ($quantidade_pg*$pagina)-$quantidade_pg;


?>

<!DOCTYPE HTML>
<html>
    
    <head>
        
        <title>NetVendas | Venda o que quiser online</title>
    
    
    
    </head>
    
    
    <body>
        <div class="container">
                 <form action="pesquisa_nome.php" method="get">
            <div class="Campo-pesquisa">
              <div class="input-group">
                  
                    <input type="text" class="form-control" placeholder="Buscar produtos..." name="nome" >
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-dark">Buscar</button>
                            
                        
                        </div>
                    
                    </div>
                
        </div><div class="form-group">
    <label for="exampleFormControlSelect1">Situação do produto</label>
    <select class="form-control" name="situacao">
      <option value="0" selected>Todos</option>
      <option value="1">Novo</option>
      <option value="2">Usado</option>
    </select>
  </div>
       <div class="form-group">
    <label for="exampleFormControlSelect1">Categoria</label>
    <select class="form-control" name="categoria">
      <option value="0" selected>Todos</option>
      <option value="2">Jogos Eletrônicos</option>
      <option value="3">Eletroeletrônicos</option>
        <option value="4">Esportes</option>
        <option value="1">Telefonia</option>
    </select>
  </div>
                </form>
            <br/><br/><br/>
            
                
            <?php 
                $i = 0;
                $totalReg = 0;
                $lista = retornaProdutosNome($incio, $quantidade_pg,$nome,$IDs,$IDc); 
                if(sizeof($lista)>0){
                    
                
                foreach($lista as $rows_produto){ 
                
           // echo ' <div class="col-sm-3"> ';
                    
                    if($i == 0){
                        echo '<div class="row">';
                        
                    }
                
                
                    
            
            ?>
                   <div class="col-sm-3">    
            <div class="card" style="width: 15rem;">
             <img class="card-img-top" src="<?php echo IMAGEM_PRODUTO_URL.$rows_produto->Imagem;?>" onerror="this.src='<?php echo IMG_URL.'Capture.JPG';?>'" width="286" height="220" >
            <div class="card-body">
                <h5 class="card-title"><?php echo $rows_produto->nomeProduto; ?></h5>
                <p class="card-text"><strong><?php echo $rows_produto->NomeSituacao; ?></strong></p>
                <p class="card-text"><strong><?php echo $rows_produto->NomeCategoria; ?></strong></p>
                <p class="card-text"><?php echo 'R$ '. $rows_produto->preco; ?></p>
                <a href="<?php echo'detalhe_produto.php?id='.$rows_produto->IDProduto; ?>" class="btn btn-primary">Ver detalhes</a>
           </div>
</div>
                
              </div>  
                <?php
                   
                     if($i == 3 || $totalReg == (sizeof($lista)-1)){
                        echo '</div>';
                        $i=0;
                         echo '<br/><br/>';
                    }
                    else{
                        $i = $i+1;
                    }
             
         
                
               
                $totalReg = $totalReg+1;
                } 
		
            	
				//Verificar a pagina anterior e posterior
				$pagina_anterior = $pagina - 1;
				$pagina_posterior = $pagina + 1;
			?>
            
         <div class="row">
            
            </div> 
            
            
            
            
            <div id="minhaPaginacao">
			<nav class="text-center">
				<ul class="pagination">
					<li class="page-item <?php echo $pagina_anterior ==0 ? ' disabled':'' ?>">
						
                        <a  class="page-link" href="pesquisa_nome.php?nome=<?php echo $nome ?>&pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span> </a>
						
				    </li>
					<?php 
					//Apresentar a paginacao
					for($i = 1; $i < $num_pagina + 1; $i++){ ?>
						<li class="page-item"><a  class="page-link" href="pesquisa_nome.php?nome=<?php echo $nome ?>&pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
					<?php } ?>
                    
                    <li class="page-item <?php echo $pagina_posterior <= $num_pagina ? '':' disabled' ?>">
						
                        <a  class="page-link" href="pesquisa_nome.php?nome=<?php echo $nome ?>&pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
								<span aria-hidden="true">&raquo;</span> </a>
						
				    </li>
                    
                    
                    
                    
                    
                    
                    
				
				</ul>
			</nav>
            </div>
        <?php
                }
            else{
                echo '<p>Ainda não temos esse produto cadastrado</p>';
            }
                
            ?>
                
                
                
     </div>
        
     
    </body>




</html>