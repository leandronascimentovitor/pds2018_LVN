-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30-Nov-2018 às 22:57
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria`
--

CREATE TABLE `categoria` (
  `IDCategoria` int(11) NOT NULL,
  `NomeCategoria` varchar(150) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria`
--

INSERT INTO `categoria` (`IDCategoria`, `NomeCategoria`) VALUES
(1, 'Telefonia'),
(2, 'Jogos Eletrônicos'),
(3, 'Eletroeletrônicos'),
(4, 'Esportes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `IDProduto` int(11) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `descricao` text NOT NULL,
  `preco` double NOT NULL,
  `IDSituacaoProduto` int(11) NOT NULL,
  `IDCategoria` int(11) NOT NULL,
  `Imagem` varchar(100) DEFAULT NULL,
  `DataAnuncio` date NOT NULL,
  `DataVenda` date DEFAULT NULL,
  `IDvendedor` int(11) NOT NULL,
  `IDComprador` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`IDProduto`, `nome`, `descricao`, `preco`, `IDSituacaoProduto`, `IDCategoria`, `Imagem`, `DataAnuncio`, `DataVenda`, `IDvendedor`, `IDComprador`) VALUES
(62, 'Homem Aranha PS4', 'Apenas venda ! \r\nEntrego no metro', 120, 2, 2, '1543518796.jpg', '2018-11-29', NULL, 10, NULL),
(63, 'Xiaomi Redmi note 5 64gb 4ram lacrado', 'Produto novo lacrado original importado \r\n-\r\nAceito cartão em até 3x sem juros ou até 12x com juros de operadora.\r\n-\r\nDesempenho incrível, acabamento Premium, câmeras com lente Samsung, bateria super duradoura de 4000mah,considerado o rei do custo X benefício.', 1150, 1, 1, '1543519156.jpg', '2018-11-29', '2018-11-29', 10, 11),
(64, 'Chuteira Adidas X 17', 'Produto novo, sem uso.', 250, 1, 4, '1543519696.jpg', '2018-11-29', NULL, 10, NULL),
(65, 'Chuteira Nike Mercurial Vapor ', 'Apenas 1 mês de uso', 350, 2, 4, '1543519963.jpg', '2018-11-29', '2018-11-29', 10, 11),
(66, 'TV Samsung 40''', 'Produto novo na caixa', 1600, 1, 3, '1543520148.jpg', '2018-11-29', NULL, 10, NULL),
(67, 'Raquete ', 'Vendo 3 raquetes \r\n\r\nNão estou usando mais.\r\n\r\n3 meses de uso', 100, 2, 4, '1543520258.jpg', '2018-11-29', '2018-11-29', 10, 11),
(68, 'Pes 2019 - XBOX ONE', 'Jogo novo lacrado', 200, 1, 2, '1543520392.jpg', '2018-11-29', '2018-11-29', 10, 11),
(69, 'Aparelho Blu-ray player Sony', 'Vendo Blu-ray Sony, 8 meses de uso.\r\nótimo estado', 200, 2, 3, '1543520506.jpg', '2018-11-29', NULL, 10, NULL),
(70, 'Fifa 19 - PS4', 'Não gostei do jogo\r\n', 189, 2, 2, '1543520614.jpg', '2018-11-29', NULL, 10, NULL),
(71, 'Smartwatch relógio inteligente K88h', 'Conheça o K88H, o verdadeiro relógio Smart Watch, com ele é possível cuidar de sua saúde graças as suas funções,os seus sensores que ajudam monitorar as funções do corpo. Além disso possuí diversas outras funções, interagindo com o seu smartphone, servindo com atalho e avisando sempre que há novas notificações nas redes sociais mais conhecidas.\r\n.O seu design avançado e inovador com pulseira de aço inoxidável, faz esse relógio passar um verdadeiro ar de elegância e te oferecendo ainda funções tecnológicas exclusivas. ', 240, 1, 4, '1543520778.jpg', '2018-11-29', '2018-11-29', 10, 0),
(72, 'Iphone x', 'Iphone novo na caixa ', 3000, 1, 1, '1543531967.jpg', '2018-11-29', NULL, 11, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `situacaoproduto`
--

CREATE TABLE `situacaoproduto` (
  `IDEstadoProduto` int(11) NOT NULL,
  `NomeSituacao` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `situacaoproduto`
--

INSERT INTO `situacaoproduto` (`IDEstadoProduto`, `NomeSituacao`) VALUES
(1, 'Novo'),
(2, 'Usado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `IDUsuario` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `senha` varchar(250) NOT NULL,
  `telefone` char(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`IDUsuario`, `nome`, `email`, `senha`, `telefone`) VALUES
(11, 'Juca', 'juca@gmail.com', '123', NULL),
(10, 'Leandro', 'leandrovitor52@hotmail.com', '9999!', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`IDCategoria`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`IDProduto`),
  ADD KEY `Situacao_Produto_fk` (`IDSituacaoProduto`),
  ADD KEY `IDCategoria_fk` (`IDCategoria`),
  ADD KEY `IDVendedor` (`IDvendedor`) USING BTREE,
  ADD KEY `IDComprador` (`IDComprador`);

--
-- Indexes for table `situacaoproduto`
--
ALTER TABLE `situacaoproduto`
  ADD PRIMARY KEY (`IDEstadoProduto`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`IDUsuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria`
--
ALTER TABLE `categoria`
  MODIFY `IDCategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `IDProduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `situacaoproduto`
--
ALTER TABLE `situacaoproduto`
  MODIFY `IDEstadoProduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `IDUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
